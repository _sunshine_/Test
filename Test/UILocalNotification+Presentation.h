//
//  UILocalNotification+Presentation.h
//  Test
//
//  Created by Maksym on 3/6/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILocalNotification (Presentation)

+ (void)createLocalNotificationWithInterval:(NSInteger)anInterval
                                      title:(NSString *)aTitle
                                       body:(NSString *)aBody;

@end
