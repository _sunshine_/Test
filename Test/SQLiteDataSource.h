//
//  SQLiteDataSource.h
//  Test
//
//  Created by Maksym on 2/28/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataSource.h"

@interface SQLiteDataSource : NSObject <DataSource>

@end
