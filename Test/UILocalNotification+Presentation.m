//
//  UILocalNotification+Presentation.m
//  Test
//
//  Created by Maksym on 3/6/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

#import "UILocalNotification+Presentation.h"

@implementation UILocalNotification (Presentation)

+ (void)createLocalNotificationWithInterval:(NSInteger)anInterval
                                      title:(NSString *)aTitle
                                       body:(NSString *)aBody
{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:anInterval];
    notification.alertTitle = aTitle;
    notification.alertBody = aBody;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

@end
