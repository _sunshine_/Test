//
//  ArrayDataSource.m
//  Test
//
//  Created by Maksym on 2/28/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

#import "ArrayDataSource.h"

@interface ArrayDataSource ()

@property (strong, nonatomic) NSMutableArray *array;

@end

@implementation ArrayDataSource

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _array = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)addItem:(NSString *)aString
{
    [self.array addObject:aString];
}

- (void)removeItemAtIndex:(NSInteger)anIndex
{
    if (anIndex >= 0 && anIndex < self.array.count)
    {
        [self.array removeObjectAtIndex:anIndex];
    }
}

- (NSInteger)countItems
{
    return self.array.count;
}

- (NSString *)itemAtIndex:(NSInteger)anIndex
{
    NSString *result = nil;
    
    if (anIndex >= 0 && anIndex < self.array.count)
    {
        result = [self.array objectAtIndex:anIndex];
    }
    
    return result;
}

@end
