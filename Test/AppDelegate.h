//
//  AppDelegate.h
//  Test
//
//  Created by Maksym on 2/28/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
