//
//  ViewController.m
//  Test
//
//  Created by Maksym on 2/28/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

#import "ViewController.h"
#import "DataSource.h"
#import "ArrayDataSource.h"
#import "SQLiteDataSource.h"
#import "UILocalNotification+Presentation.h"

static NSString * const kViewControllerTableCellIdentifier = @"cell";

typedef NS_ENUM(NSInteger, DataSourceType)
{
    kDataSourceTypeArray,
    kDataSourceTypeSQLite
};

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) id<DataSource> dataSource;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSInteger index = self.segmentedControl.selectedSegmentIndex;
    
    [self createDataSourceWithType:index];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:kViewControllerTableCellIdentifier];
    
    cell.textLabel.text = [self.dataSource itemAtIndex:anIndexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)aSection
{
    return [self.dataSource countItems];
}

- (IBAction)onButtonClick:(UIButton *)aSender
{
    NSString *str = self.textField.text;
    
    [self.dataSource addItem:str];
    self.textField.text = @"";
    [self.tableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.dataSource countItems] - 1)  inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [UILocalNotification createLocalNotificationWithInterval:0
                                                       title:@"New Item"
                                                        body:str];
}

- (IBAction)onSegmentedControlClick:(UISegmentedControl *)aSender
{
    NSInteger index = aSender.selectedSegmentIndex;
    
    [self createDataSourceWithType:index];
    [self.tableView reloadData];
}


- (BOOL)tableView:(UITableView *)aTableView canEditRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    return YES;
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)anEditingStyle forRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    if (anEditingStyle == UITableViewCellEditingStyleDelete)
    {
        NSInteger index = anIndexPath.row;
        NSString *str = [self.dataSource itemAtIndex:index];
        
        [self.dataSource removeItemAtIndex:index];
        [self.tableView reloadData];
        
        [UILocalNotification createLocalNotificationWithInterval:0
                                                           title:@"Deleted Item"
                                                            body:str];
    }
}

- (void)createDataSourceWithType:(DataSourceType)aType
{
    id<DataSource> dataSource = nil;
    
    if (aType == kDataSourceTypeArray)
    {
        dataSource = [[ArrayDataSource alloc] init];
    }
    else if (aType == kDataSourceTypeSQLite)
    {
        dataSource = [[SQLiteDataSource alloc] init];
    }
    
    self.dataSource = dataSource;
}

@end
