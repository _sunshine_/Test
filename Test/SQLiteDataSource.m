//
//  SQLiteDataSource.m
//  Test
//
//  Created by Maksym on 2/28/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

#import "SQLiteDataSource.h"
#import <sqlite3.h>

static NSString * const kSQLiteDataSourceDatabaseName = @"testAppDB.db";

@interface SQLiteDataSource ()

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *database;

@end

@implementation SQLiteDataSource

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        _databasePath = [[NSString alloc] initWithFormat:@"%@/%@", docsDir, kSQLiteDataSourceDatabaseName];
        
        NSFileManager *fs = [NSFileManager defaultManager];
        
        if (![fs fileExistsAtPath:_databasePath])
        {
            if (sqlite3_open([_databasePath UTF8String], &_database) == SQLITE_OK)
            {
                char *errorMessage;
                const char *sql_statement = "CREATE TABLE IF NOT EXISTS datasource (id INTEGER PRIMARY KEY AUTOINCREMENT, stringvalue TEXT)";
                
                if (sqlite3_exec(_database, sql_statement, NULL, NULL, &errorMessage) == SQLITE_OK)
                {
                    NSLog(@"%@", @"Table was created in the database.");
                }
                else
                {
                    NSLog(@"%@", @"Table can not be created in the database.");
                }
                
                sqlite3_close(_database);
            }
        }
    }
    
    return self;
}

- (void)addItem:(NSString *)aString
{
    const char *dbPath = [self.databasePath UTF8String];
    sqlite3 *database = self.database;
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        const char *query = "INSERT INTO datasource (stringvalue) VALUES (?)";
        
        if (sqlite3_prepare_v2(database, query, -1, &statement, NULL) == SQLITE_OK)
        {
            sqlite3_bind_text(statement, 1, [aString UTF8String], -1, SQLITE_TRANSIENT);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"%@", @"The item was added to the database");
            }
            else
            {
                NSLog(@"%@", @"Failed to add the item to the database");
            }
        }
        else
        {
            NSLog(@"%@", @"Failed while creating statement");
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
}

- (void)removeItemAtIndex:(NSInteger)anIndex
{
    NSInteger itemID = [self itemIDAtIndex:anIndex];
    const char *dbPath = [self.databasePath UTF8String];
    sqlite3 *database = self.database;
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        const char *query = "DELETE FROM datasource WHERE ID = (?)";
        
        if (sqlite3_prepare_v2(database, query, -1, &statement, NULL) == SQLITE_OK)
        {
            sqlite3_bind_int(statement, 1, (int)itemID);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"%@", @"Item was deleted from the database");
            }
            else
            {
                NSLog(@"%@", @"Failed to delete item from the database");
            }
        }
        else
        {
            NSLog(@"%@", @"Failed while creating statement");
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
}

- (NSInteger)countItems
{
    NSInteger result = 0;
    const char *dbPath = [self.databasePath UTF8String];
    sqlite3 *database = self.database;
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        const char *query = "SELECT COUNT(*) FROM datasource";
        
        if (sqlite3_prepare_v2(database, query, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = sqlite3_column_int(statement, 0);
                NSLog(@"%@ %li", @"Item was found in the database - ", result);
            }
            else
            {
                NSLog(@"%@", @"Item was not found in the database");
            }
        }
        else
        {
            NSLog(@"%@", @"Failed while creating statement");
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    return result;
}

- (NSString *)itemAtIndex:(NSInteger)anIndex
{
    NSString *result = nil;
    const char *dbPath = [self.databasePath UTF8String];
    sqlite3 *database = self.database;
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        const char *query = "SELECT stringvalue FROM datasource ORDER BY ID LIMIT 1 OFFSET (?)";
        
        if (sqlite3_prepare_v2(database, query, -1, &statement, NULL) == SQLITE_OK)
        {
            sqlite3_bind_int(statement, 1, (int)anIndex);
            
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                const char *queryResult = (const char *)sqlite3_column_text(statement, 0);
                result = [[NSString alloc] initWithUTF8String:queryResult];
                NSLog(@"%@ %@", @"Item was found in the database - ", result);
            }
            else
            {
                NSLog(@"%@", @"Item was not found in the database");
            }
        }
        else
        {
           NSLog(@"%@", @"Failed while creating statement");
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    return result;
}

- (NSInteger)itemIDAtIndex:(NSInteger)anIndex
{
    NSInteger result = 0;
    const char *dbPath = [self.databasePath UTF8String];
    sqlite3 *database = self.database;
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        const char *query = "SELECT id FROM datasource ORDER BY ID LIMIT 1 OFFSET (?)";
        
        if (sqlite3_prepare_v2(database, query, -1, &statement, NULL) == SQLITE_OK)
        {
            sqlite3_bind_int(statement, 1, (int)anIndex);
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = sqlite3_column_int(statement, 0);
                NSLog(@"%@ %li", @"Item ID was found in the database - ", result);
            }
            else
            {
                NSLog(@"%@", @"Item ID was not found in the database");
            }
        }
        else
        {
            NSLog(@"%@", @"Failed while creating statement");
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
    return result;
}

@end
