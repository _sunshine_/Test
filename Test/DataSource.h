//
//  DataSource.h
//  Test
//
//  Created by Maksym on 2/28/17.
//  Copyright (c) 2017 Maksym. All rights reserved.
//

@protocol DataSource <NSObject>

- (void)addItem:(NSString *)aString;
- (void)removeItemAtIndex:(NSInteger)anIndex;
- (NSInteger)countItems;
- (NSString *)itemAtIndex:(NSInteger)anIndex;

@end
